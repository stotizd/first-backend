const express = require('express');
const path = require('path');
const app = express();
const exphbs = require('express-handlebars');
const coursesRoutes = require('./routes/courses')
const addRoutes = require('./routes/add')
const homeRoutes = require('./routes/home')
const port = process.env.PORT ?? 8000;


const hbs = exphbs.create({
  	defaultLayout: 'main',
  	extname: 'hbs'
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'pages')
app.use('/', express.static('static'));

app.use(express.urlencoded({extended: true}))

app.use('/', homeRoutes);
app.use('/courses', coursesRoutes);
app.use('/add', addRoutes);



app.listen(port, () => {
	console.log(`Server started, port ${port}`)

});
